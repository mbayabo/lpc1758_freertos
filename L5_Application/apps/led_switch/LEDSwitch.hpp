/*
 * LEDSwitch.cpp
 *
 *  Created on: Sep 8, 2017
 *      Author: mbayabo
 */

#include "eint.h"
#include "io.hpp"
#include "gpio.hpp"

bool switch_is_on = false;

// Switch board control signal to LED board
GPIO switch_out(P2_6);

// LED controls for LED board
GPIO led_out(P2_5);
GPIO led_in(P2_7);

void switch_on(void) {
    switch_is_on = true;
}

void switch_off(void) {
    switch_is_on = false;
}

void led_setup(void) {

    /*
     * Switch Board
     */

    // set P2.0 as the interrupt switch for switch board
    eint3_enable_port2(0, eint_rising_edge, switch_on);
    eint3_enable_port2(0, eint_falling_edge, switch_off);

    // set P2.3 as the output switch for switch board
    switch_out.setAsOutput();

    /*
     * LED Board
     */

    // set P2.6 as the output pin for led board
    led_out.setAsOutput();

    // set P2.7 as the input pin for led board
    led_in.setAsInput();
}
