# GPIO lab

* Interface an external LED (such as P2.1) to "Board A", which we will refer to as "LED Board"
* Interface an external switch (such as P2.2) to "Board B", which we will refer to as "SW Board" (switch board)
* Connect the LED Board and SW Board together using "Port 2"
    * Example: P2.0 of LED board connects to P2.0 on the SW Board
    * Do not forget common ground
* LED board Software is simple, whenever P2.0 (which is interfaced to the SW Board) is detected HIGH, light up its LED (on P2.1), else turn it off
* On the SW Board, use "External Interrupt" API (eint.h) to detect when the switch gets pressed
    * The callback should simply turn a flag (global variable) to true
    * The callback MUST NOT POLL, and MUST NOT DELAY, and EXIT IMMEDIATELY since it is an interrupt function
    * Use period_init() function to initialize the callback. This initialization function is only called once before starting the RTOS
* On the SW Board, use the periodic callbacks, and when the flag is true, toggle the P2.0 for at least 500ms
    * Turn on the periodic scheduler at main.cpp
    * Deploy your code to periodic_callbacks.cpp (like 10Hz function)
* Bonus points: Use a binary semaphore in the interrupt, and have the 10Hz task wait on this semaphore with 0 ms block time. See the FreeRTOS video about the binary semaphore
    * Note that if you use a periodic callback, you will have to use zero block time for the semaphore since you do not want to block in a period function.
* For the demo:
    * The SW Board should detect its switch press by an external interrupt and toggle its P2.0
    * The LED Board should detect its own P2.0 and light up the LED* 